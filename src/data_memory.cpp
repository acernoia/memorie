#include <systemc.h>
#include "data_memory.hpp"

using namespace std;

void data_memory::memory()
{
   static const unsigned MEM_DELAY = 0; //ns
   static const unsigned MEM_SIZE = 1024;
   unsigned indirizzo,dato;
   unsigned short line[MEM_SIZE];
   //Memoria inizializzata a zero
   for (unsigned i=0;i<MEM_SIZE;i++) 
   {
       line[i]=0;
   }
   cout << "MEMORIA DATI INIZIALIZZATA A ZERO" << endl << endl;
   while(true)
   {
      //Memoria pronta
      ready->write(1);
      //Aspetta gli ingressi
      wait();
      wait(10,SC_NS);
      //Se enable vale 1 scrivi
      if (enable->read()==1)
      {
         ready->write(0);
         line[address->read()]=datain->read();
         cout << "   MEM[" << address->read() << "] <= " << datain->read() << endl;
         wait(MEM_DELAY,SC_NS);
         ready->write(1);
      }
      //altrimenti leggi
      else
      {
         indirizzo=address->read();
         if (indirizzo<MEM_SIZE)
            dato=line[indirizzo];
         else
            dato=0;
         dataout->write(dato);
      }
   }
}
