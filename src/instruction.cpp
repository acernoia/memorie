#include <systemc.h>
#include "instruction.hpp"

using namespace std;

void instruction::decodifica()
{
   sc_bv<16> istruzione;
   sc_bv<3>  mask_rA, mask_rB, mask_rC, mask_rOP;
   sc_bv<7>  mask_lsb7;
   sc_bv<10> mask_lsb10;
   while(true)
   {
     wait(); 
     istruzione=ingresso->read();
     mask_rA=istruzione.range(12,10);
     mask_rB=istruzione.range(9,7);
     mask_rC=istruzione.range(2,0);
     mask_rOP=istruzione.range(15,13);
     mask_lsb7=istruzione.range(6,0);
     mask_lsb10=istruzione.range(9,0);
     rA->write(mask_rA);
     rB->write(mask_rB);
     rC->write(mask_rC);
     rOP->write(mask_rOP);
     lsb7->write(mask_lsb7);
     lsb10->write(mask_lsb10);
   }

}
