#include <systemc.h>
#include <fstream>
#include <iostream>
#include <string>
#include "programmatore.hpp"

using namespace std;

sc_event fine_programmazione;

static const unsigned MEM_SIZE = 64;
sc_bv<16> line[MEM_SIZE];
sc_uint<16> halt_line=0;
void programmatore::leggi()
{
    halt_signal->write(0);
    wait(fine_programmazione);
    while(true)
    {
      wait();
      istruzione->write(line[indirizzo->read()]);
      if (indirizzo->read()==halt_line)
          halt_signal->write(1);
    }
}

void programmatore::programma()
{
    int i=0;
    int k=0;
    int indirizzo=1;
    ifstream f("../assembly/assembly.txt");
    string s;
    string operazione;
    string operando1, operando2, operando3;
    char immediato_char[10];
    sc_bv<3> opcode, rA, rB, rC;
    sc_bv<7> immediato7_bv;
    sc_bv<10> immediato10_bv;
    sc_uint<7> immediato7;
    sc_uint<10> immediato10;
    sc_bv<16> istruzione_da_scrivere;
    if(!f)
    {
        cout << "Il file non esiste!" << endl;
    }

    while(f.good())
    {
        getline(f, s); //legge tutta la riga dal file e la mette nella variabile s
        i=0;
        k=0;
        //cout << "stringa letta da file:" << s << endl;
        while( (s[i]!=' ') && (s[i]!='\0') ) //legge il campo operazione
        {
           operazione.push_back(s[i]);
           i++;
        }
       
        //cout << "operazione letta da file: " << operazione << endl;
        if ((operazione!="LUI")&&(operazione!="JALR"))
        {
           i++; //salta uno spazio
           while(s[i]!=' ') //legge operando1
           {
              operando1.push_back(s[i]);
              i++;
           }
           i++; //salta uno spazio
           while(s[i]!=' ') //legge operando2
           {
              operando2.push_back(s[i]);
              i++;
           }
           i++; //salta uno spazio
           while( (s[i]!=' ') && (s[i]!='\0') ) //legge operando3
           {
              operando3.push_back(s[i]);
              immediato_char[k]=s[i];
              i++; k++;
           }
           immediato_char[k]='\0';
        //cout << "operando1 letto:" << operando1 << endl;
        //cout << "operando2 letto:" << operando2 << endl;
        //cout << "operando3 letto:" << operando3 << endl;
        }
        else if ((operazione=="LUI")||(operazione=="JALR"))
        {
           i++; //salta uno spazio
           while(s[i]!=' ') //legge operando1
           {
              operando1.push_back(s[i]);
              i++;
           }
           i++; //salta uno spazio
           while( (s[i]!=' ') && (s[i]!='\0') ) //legge operando2
           {
              operando2.push_back(s[i]);
              immediato_char[k]=s[i];
              i++; k++;
           }
           immediato_char[k]='\0';
            //cout << "operando1 letto:" << operando1 << endl;
            //cout << "operando2 letto:" << operando2 << endl;
        }

        if (operazione=="HALT")
        {
           //cout << "Operazione HALT trovata" << endl;
           halt_line=indirizzo;
        }

        if (operazione=="ADD")
        {
           opcode="000";
           rA=operando1[1]-'0';
           rB=operando2[1]-'0';
           rC=operando3[1]-'0';
           istruzione_da_scrivere=(opcode,rA,rB,"0000",rC);
           //cout << "istruzione da scrivere:" << istruzione_da_scrivere << endl;
        }

        if (operazione=="ADDI")
        {
           opcode="001";
           rA=operando1[1]-'0';
           rB=operando2[1]-'0';
           immediato7=atoi(immediato_char);
           immediato7_bv=immediato7;
           istruzione_da_scrivere=(opcode,rA,rB,immediato7_bv);
           //cout << "istruzione da scrivere:" << istruzione_da_scrivere << endl;
        }

        if (operazione=="NAND")
        {  
           opcode="010";
           rA=operando1[1]-'0';
           rB=operando2[1]-'0';
           rC=operando3[1]-'0';
           istruzione_da_scrivere=(opcode,rA,rB,"0000",rC);
           //cout << "istruzione da scrivere:" << istruzione_da_scrivere << endl;
        } 

        if (operazione=="LUI")
        {  
           opcode="011";
           rA=operando1[1]-'0';
           immediato10=atoi(immediato_char);
           immediato10_bv=immediato10;
           istruzione_da_scrivere=(opcode,rA,immediato10_bv);
           //cout << "istruzione da scrivere:" << istruzione_da_scrivere << endl;
        } 

        if (operazione=="SW")
        {  
           opcode="101";
           rA=operando1[1]-'0';
           rB=operando2[1]-'0';
           immediato7=atoi(immediato_char);
           immediato7_bv=immediato7;
           istruzione_da_scrivere=(opcode,rA,rB,immediato7_bv);
           //cout << "istruzione da scrivere:" << istruzione_da_scrivere << endl;
        } 

        if (operazione=="LW")
        {  
           opcode="100";
           rA=operando1[1]-'0';
           rB=operando2[1]-'0';
           immediato7=atoi(immediato_char);
           immediato7_bv=immediato7;
           istruzione_da_scrivere=(opcode,rA,rB,immediato7_bv);
           //cout << "istruzione da scrivere:" << istruzione_da_scrivere << endl;
        } 

        if (operazione=="BEQ")
        {  
           opcode="110";
           rA=operando1[1]-'0';
           rB=operando2[1]-'0';
           immediato7=atoi(immediato_char);
           immediato7_bv=immediato7;
           istruzione_da_scrivere=(opcode,rA,rB,immediato7_bv);
           //cout << "istruzione da scrivere:" << istruzione_da_scrivere << endl;
        } 

        if (operazione=="JALR")
        {  
           opcode="111";
           rA=operando1[1]-'0';
           rB=operando2[1]-'0';
           istruzione_da_scrivere=(opcode,rA,rB,"0000000");
           //cout << "istruzione da scrivere:" << istruzione_da_scrivere << endl;
        } 

        if (operazione=="NOP")
        {
           istruzione_da_scrivere=0;
        }
        line[indirizzo]=istruzione_da_scrivere;
        //cout << "Indirizzo                     : " << indirizzo << endl;
        //cout << "Istruzione scritta in memoria : " << line[indirizzo] << endl << endl;
        indirizzo++;

        operazione.clear();
        operando1.clear();
        operando2.clear();
        operando3.clear();
        s.clear();
    }
    f.close();
    cout << "CARICAMENTO ISTRUZIONI COMPLETATO" << endl << endl;
   fine_programmazione.notify(SC_ZERO_TIME);
}
