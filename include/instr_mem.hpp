#ifndef INSTR_MEM_HPP
#define INSTR_MEM_HPP

SC_MODULE(instr_mem)
{
   sc_in<sc_uint<16> >  indirizzo;
   sc_out<sc_uint<16> > istruzione;

   SC_CTOR(instr_mem)
   {
     SC_THREAD(leggi);
     sensitive << indirizzo;
   }
   private:
   void leggi ();
};


#endif
