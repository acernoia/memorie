#ifndef PROGRAMMATORE_HPP
#define PROGRAMMATORE_HPP

SC_MODULE(programmatore)
{
   sc_in<sc_uint<16> >  indirizzo;
   sc_out<sc_uint<16> > istruzione;
   sc_out<sc_uint<1> >  halt_signal;

   SC_CTOR(programmatore)
   {
     SC_THREAD(programma);
     SC_THREAD(leggi);
     sensitive << indirizzo;
   }
   private:
   void leggi ();
   void programma();
};


#endif
