#ifndef INSTRUCTION_HPP
#define INSTRUCTION_HPP

SC_MODULE(instruction)
{
   sc_in<sc_uint<16> >   ingresso;
   sc_out<sc_uint<3> >   rA;
   sc_out<sc_uint<3> >   rB;
   sc_out<sc_uint<3> >   rC;
   sc_out<sc_uint<3> >   rOP;
   sc_out<sc_uint<7> >   lsb7;
   sc_out<sc_uint<10> >  lsb10;

   SC_CTOR(instruction)
   {
     SC_THREAD(decodifica);
     sensitive << ingresso;
   }
   private:
   void decodifica ();
};


#endif
