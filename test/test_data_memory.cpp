#include <systemc.h>
#include <string>
#include "data_memory.hpp"

using namespace std;

SC_MODULE(TestBench)
{
   sc_signal<sc_uint<16> > address_s;
   sc_signal<sc_uint<16> > datain_s;
   sc_signal<sc_uint<1> >  enable_s;
   sc_signal<sc_uint<16> > dataout_s;
   sc_signal<sc_uint<1> >  ready_s;

   data_memory MEM;
   
   SC_CTOR(TestBench) : MEM("MEM")
   {
        SC_THREAD(stimulus_thread);
        MEM.address(this->address_s);
        MEM.datain(this->datain_s);
        MEM.enable(this->enable_s);
        MEM.dataout(this->dataout_s);
        MEM.ready(this->ready_s);
        init_values();
   }
  
   int check()
   {
      for (unsigned i=0;i<MEM_SIZE;i++) 
      {
        if (dato_letto[i]!=in_test[i])
        {
	    cout << "TEST FALLITO: " << i << endl;
            cout << "RISULTATO TEORICO: " << i+1 << endl;
            cout << "RISULTATO TEST :" << dato_letto[i] << endl;
            return 1;
        }
      }
      cout << "TEST OK" << endl;
      return 0;
   }

   private:

   void stimulus_thread() 
   {
     cout << "START STIMULUS" << endl << endl;
     for (unsigned i=0;i<MEM_SIZE;i++) 
     {

       while(ready_s.read()==0)
       {
         wait(1,SC_NS);
       }

       enable_s.write(1);
       address_s.write(i);
       datain_s.write(in_test[i]);

       wait(1,SC_NS);
       if (i==0||i==MEM_SIZE-1)
          cout << "Scritto : " << datain_s.read() << " in " << address_s.read() << endl;
       wait(20,SC_NS);
       while(ready_s.read()==0)
       {
         wait(1,SC_NS);
       }

       enable_s.write(0);

       wait(20,SC_NS);

       while(ready_s.read()==0)
       {
         wait(1,SC_NS);
       }

       dato_letto[i]=dataout_s.read();
       if (i==0||i==MEM_SIZE-1)
          cout << "Letto   : " << dataout_s.read() << " da " << address_s.read() << endl << endl;
     }
   }

   static const unsigned MEM_SIZE = 1024;
   unsigned short in_test[MEM_SIZE];
   unsigned short dato_letto[MEM_SIZE];
   void init_values()
   {
     for (unsigned i=0;i<MEM_SIZE;i++) 
     {
       in_test[i]=4*i;
     }
   }
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
} 
