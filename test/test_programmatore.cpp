#include <systemc.h>
#include <string>
#include "programmatore.hpp"

using namespace std;
sc_event fine_programmazione;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > indirizzo,istruzione;
    sc_signal<sc_uint<1> > halt_signal; 

    programmatore prog;
    
    SC_CTOR(TestBench) : prog("prog")
    {
        SC_THREAD(stimulus_thread);
        prog.indirizzo(this->indirizzo);
        prog.istruzione(this->istruzione);
        prog.halt_signal(this->halt_signal);
        init_values();
    }

    int check() 
    {
        cout << "FINE TEST" << endl;
        return 0;
    }


  private:

   void stimulus_thread() 
   {
       cout << "START STIMULUS" << endl;
       wait(fine_programmazione);
       for(int i=0;i<TEST_SIZE;i++)
       {
          indirizzo.write(i);
          wait(1,SC_NS);
          cout << "Indirizzo  :" << indirizzo.read() << endl;
          cout << "Istruzione :" << istruzione.read() << endl;
          cout << "Halt signal:" << halt_signal.read() << endl << endl;
       }
    }

    const int TEST_SIZE=10;
    void init_values() 
    {
  
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START" << endl << endl;

  sc_start();

  return test.check();
}
